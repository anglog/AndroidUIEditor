﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Windows.Forms.Design;
using System.Windows.Forms;
using System.Drawing;
using System.IO;
using UIEditor.Entity;
using UIEditor.Component;
using Utils;

namespace UIEditor.PropertyGridEditor
{
    public class PropertyGridStringImageEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            try
            {
                ViewNode vNode = context.Instance as ViewNode;
                if (null == vNode)
                {
                    return null;
                }

                IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
                if (edSvc != null)
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = MyConst.PicFilter;
                    ofd.InitialDirectory = MyCache.ProjectResImgDir;
                    if (DialogResult.OK == ofd.ShowDialog())
                    {
                        value = ProjResManager.CopyImageSole(ofd.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine("PropertyGridImageEditor Error : " + ex.Message);
                return value;
            }
            return value;
        }

        public override bool GetPaintValueSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override void PaintValue(PaintValueEventArgs e)
        {
            base.PaintValue(e);

            ViewNode vNode = e.Context.Instance as ViewNode;
            if (null == vNode)
            {
                return;
            }

            Graphics g = e.Graphics;

            try
            {
                string imageFile = Path.Combine(MyCache.ProjImgPath, (string)e.Value);
                //string imageFile = Path.Combine(vNode.ImagePath, (string)e.Value);
                Image img = ImageHelper.GetDiskImage(imageFile);
                g.DrawImage(img, new Rectangle(1, 1, 20, 14));
            }
            catch (Exception ex)
            {
                Console.WriteLine("PropertyGridImageEditor Error:" + ex.Message);
            }
        }
    }
}
