﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Structure.Control
{
    public class KNXWebCamer : KNXControlBase
    {
        public string Symbol { get; set; }
        public string serviceURL { get; set; }    // 网址
        public string username { get; set; }    // 用户名
        public string password { get; set; }    // 密码

    }
}
