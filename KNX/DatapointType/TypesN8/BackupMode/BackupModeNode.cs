﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KNX.DatapointType.TypesN8.BackupMode
{
    class BackupModeNode:TypesN8Node
    {
        public BackupModeNode()
        {
            this.KNXSubNumber = DPST_121;
            this.DPTName = "backup mode";
        }

        public static TreeNode GetTypeNode()
        {
            BackupModeNode nodeType = new BackupModeNode();
            nodeType.Text = nodeType.KNXMainNumber + "." + nodeType.KNXSubNumber + " " + nodeType.DPTName;

            return nodeType;
        }
    }
}
